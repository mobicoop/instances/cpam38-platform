export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#0c419a',
      accent: '#1cb0b9',
      secondary: '#e11a81',
      success: '#6cbe1a',
      info: '#e67261',
      warning: '#f0b323',
      error: '#ef3e42'
    },
    dark: {
      primary: '#0c419a',
      accent: '#1cb0b9',
      secondary: '#e11a81',
      success: '#6cbe1a',
      info: '#e67261',
      warning: '#f0b323',
      error: '#ef3e42'
    }
  }
}
